package fizzbuzztest;

public class FizzBuzz {
    public String convert(int number) {
        boolean isMultipleOfFive = number % 5 == 0;
        boolean isMultipleOfThree = number % 3 ==0;
        if( isMultipleOfFive && isMultipleOfThree)
        {
            return "FizzBuzz";
        }
       if( isMultipleOfThree )
           return "Fizz";
       return "Buzz";
    }
}



