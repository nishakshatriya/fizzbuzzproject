package fizzbuzztest;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FizzBuzzTest {
    @Test
    public void shouldReturnFizzIfMultipleOf3() {
        final FizzBuzz fizzBuzz = new FizzBuzz();
        final String convertNumber = fizzBuzz.convert(6);
        assertThat(convertNumber, is("Fizz"));
    }

    @Test
    public void shouldReturnBuzzIfMultipleOf5() {
        final FizzBuzz fizzBuzz = new FizzBuzz();
        final String convertNumber = fizzBuzz.convert(10);
        assertThat(convertNumber, is("Buzz"));
    }

    @Test
    public void shouldReturnFizzBuzzIfMultipleOfBoth() {
        final FizzBuzz fizzBuzz = new FizzBuzz();
        final String convertNumber = fizzBuzz.convert(30);
        assertThat(convertNumber, is("FizzBuzz"));
    }
}
